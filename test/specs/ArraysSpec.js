define([
  'Arrays',
  'Types'
], function(Arrays, Types) {
  describe('Arrays', function() {

    it('can be cloned', function() {
      var array = [1, 2, 3];
      var array2 = Arrays.clone(array);
      array.push(4);
      expect(array.length).toEqual(4);
      expect(array2.length).toEqual(3);
    });

    it('can find differences', function() {
      var a = [1, 2, 3];
      var b = [2];
      expect(Arrays.difference(a, b), [1, 3]);
    });

    it('can convert things to arrays', function() {
      function testArgs() {
        expect(Types.isArrayLiteral(arguments)).toBe(false);
        var args = Arrays.clone(arguments);
        expect(Types.isArrayLiteral(args)).toBe(true);
      }
      testArgs();
    });

  });
});
