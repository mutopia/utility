define([
  'Class',
  'Log',
  'Testing',
], function(Class, Log, T) {
  var animalProto = {
    _name: null,

    _hello: null,

    _move: null,

    _init: function(name) {
      this._name = name;
    },

    greet: function() {
      return this._hello;
    },

    move: function() {
      return this._move;
    }
  };

  var catProto = {
    _init: function(name) {
      this._super(name);
      this._hello = 'meow';
      this._move = 'slinks';
    },
  };

  var redProto = {
    _colour: 'red'
  };

  var dogProto = {

    _init: function(name) {
      this._super(name);
    },

    greet: function() {
      return 'bark';
    },

    move: function() {
      return 'bounds';
    }
  };

  var lycanthropyProto = {
    _shifted: false,

    _init: function(name) {
      this._super(name);
      this._greetL = 'hello';
      this._moveL = 'walk';
    },

    greet: function() {
      return this._shifted ? this._super() : this._greetL;
    },

    move: function() {
      return this._shifted ? this._super() : this._moveL;
    },

    shift: function() {
      this._shifted = !this._shifted;
    }
  };

  var foodProto = {
    _name: null,

    _delicious: false,

    _init: function(name, delicious) {
      this._name = name;
      this._delicious = delicious;
    }
  };

  var Animal;
  var Cat;
  var Dog;
  var Lycanmorph;
  var Weredog;
  var Food;
  var Red;

  describe('A Class', function() {

    beforeEach(function() {
      // Log.off();
      Lycanmorph = Class.extend(lycanthropyProto);
      Animal = Class.extend(animalProto);
      Cat = Animal.extend(catProto);
      Dog = Animal.extend(dogProto);
      Red = Class.extend(redProto);
    });

    afterEach(function() {
      Animal = Cat = Dog = Lycanmorph = Weredog = null;
      // Log.off();
    });

    describe('Simple extension', function() {

      it('can chain extend on concrete classes', function() {
        Lycanmorph = Class.extend(lycanthropyProto);
        Animal = Class.extend(animalProto);
        Cat = Animal.extend(catProto);
        Dog = Animal.extend(dogProto);
        Red = Class.extend(redProto);
        expect(Lycanmorph).toBeDefined();
        expect(Animal).toBeDefined();
        expect(Cat).toBeDefined();
        expect(Dog).toBeDefined();
        expect(Red).toBeDefined();
      });

      it('can construct new instances from constructors', function() {
        var animal = new Animal('bob');
        expect(animal).toBeDefined();
        expect(animal instanceof Animal).toBe(true);
        expect(animal._name).toEqual('bob');
      });

      it('can extend superclasses via the _init method', function() {
        Animal = Class.extend(animalProto);
        Cat = Animal.extend(catProto);
        var cat = new Cat('Felix');
        expect(cat).toBeDefined();
        expect(cat instanceof Cat).toBe(true);
        expect(cat instanceof Animal).toBe(true);
        expect(cat._name).toEqual('Felix');
        expect(cat.greet()).toEqual('meow');
        expect(cat.move()).toEqual('slinks');
      });

      it('can be extend superclasses via instance methods', function() {
        var dog = new Dog('Archie');
        expect(dog).toBeDefined();
        expect(dog instanceof Dog).toBe(true);
        expect(dog instanceof Animal).toBe(true);
        expect(dog._name).toEqual('Archie');
        expect(dog.greet()).toEqual('bark');
        expect(dog.move()).toEqual('bounds');
      });

    });

    describe('Multiple Inheritance', function() {
      it('can maintain the inheritance chain when a intermediary super class does not contain an' +
          'a function/method from higher up the chain that should be inherited', function() {

        // Construct a red weredog that doesn't contain any of the Animal methods.
        var Weredog = Animal.extend([Dog, Red], lycanthropyProto);

        // We should still be able to construct the Weredog even though Red does not contain
        // _init(), greet() or move(). These should be inherited from Dog.
        var lupin = new Weredog('lupin');
        expect(lupin instanceof Weredog).toBe(true);
        expect(lupin.greet()).toEqual('hello');
        expect(lupin.move()).toEqual('walk');
        lupin.shift();
        expect(lupin.greet()).toEqual('bark');
        expect(lupin.move()).toEqual('bounds');
      });

      it('can perform single inheritance when the class hierarchy is null', function() {
        Food = Class.extend(null, foodProto);
        var cat = new Food('cheese', true);
        expect(cat).toBeDefined();
        expect(cat instanceof Food).toBe(true);
      });

      it('can perform single inheritance when the class hierarchy is empty', function() {
        Food = Class.extend([], foodProto);
        var cat = new Food('Chocolate', true);
        expect(cat).toBeDefined();
        expect(cat instanceof Food).toBe(true);
      });

      it('can chain class extends', function() {
        var Weredog = Dog.extend(lycanthropyProto);
        var lupin = new Weredog('lupin');
        expect(lupin).toBeDefined();
        expect(lupin instanceof Animal).toBe(true);
        expect(lupin instanceof Dog).toBe(true);
        expect(lupin instanceof Weredog).toBe(true);
        expect(lupin.greet()).toEqual('hello');
        expect(lupin.move()).toEqual('walk');
        lupin.shift();
        expect(lupin.greet()).toEqual('bark');
        expect(lupin.move()).toEqual('bounds');
        var RedWeredog = Weredog.extend(redProto);
        lupin = new RedWeredog('lupin');
        expect(lupin instanceof Animal).toBe(true);
        expect(lupin instanceof Dog).toBe(true);
        expect(lupin instanceof Weredog).toBe(true);
        expect(lupin instanceof RedWeredog).toBe(true);
      });

      it('can chain class extends as mixins', function() {
        var Weredog = Class.extend([Dog, Red], lycanthropyProto);
        var lupin = new Weredog('lupin');
        expect(lupin).toBeDefined();
        expect(lupin instanceof Animal).toBe(true);
        expect(lupin instanceof Dog).toBe(true);
        expect(lupin instanceof Weredog).toBe(true);

        expect(lupin.greet()).toEqual('hello');
        expect(lupin.move()).toEqual('walk');
        expect(lupin._colour).toEqual('red');
        lupin.shift();
        expect(lupin.greet()).toEqual('bark');
        expect(lupin.move()).toEqual('bounds');
      });

      it('can chain class extended with mixins', function() {
        Log.on();
        var Zombie = Class.extend({_brains: false});
        var Weredog = Class.extend([Dog, Red, Zombie], lycanthropyProto);
        var ZombieWeredog = Weredog.extend({});
        var lupin = new ZombieWeredog('zombie-lupin');
        expect(lupin).toBeDefined();
        expect(lupin instanceof Animal).toBe(true, 'is Animal');
        expect(lupin instanceof Dog).toBe(true, 'is Dog');
        expect(lupin instanceof Weredog).toBe(true, 'is Weredog');
        // Check that Zombie is inherited.
        expect(lupin._brains).toBe(false);
      });

    });

    describe('Error Handling', function() {

      it('throws an error when the given prototype is actually a Class', function() {
        expect(function() {
          Class.extend([], {extend: function() {}});
        }).toThrow();
        expect(function() {
          Class.extend({extend: function() {}});
        }).toThrow();
      });

      it('throws an error when the prototype is not provided', function() {
        expect(function() {
          Class.extend([], null);
        }).toThrow();

        expect(function() {
          Class.extend([]);
        }).toThrow();

        expect(function() {
          Class.extend();
        }).toThrow();

        expect(function() {
          Class.extend(null);
        }).toThrow();
      });

      it('throws an error when there is an undefined or null subclass prototype in the class' +
          ' hierarchy', function() {

        expect(function() {
          Class.extend([null, Dog], {});
        }).toThrow();
        expect(function() {
          Class.extend([Dog, null], {});
        }).toThrow();
        expect(function() {
          Class.extend([Cat, undefined, Dog], {});
        }).toThrow();
        expect(function() {
          Class.extend([undefined], {});
        }).toThrow();
        expect(function() {
          Class.extend([null], {});
        }).toThrow();
      });

    });

  });

});
