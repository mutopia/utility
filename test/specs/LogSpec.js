define([
  'Log',
], function(Log) {
  describe('A Log', function() {

    beforeEach(function() {
      Log.on();
      Log.setLevel('debug');
    });

    afterEach(function() {
      Log.off();
    });

    it('should be able to log', function() {
      Log.debug('Debug');
      Log.error('Error');
      Log.warn('Warn');
    });

    it('can be disabled', function() {
      Log.off();
      Log.debug('Should\'t Debug');
      Log.error('Should\'t Error');
      Log.warn('Should\'t Warn');
      Log.on();
      Log.debug('Should Debug');
      Log.error('Should Error');
      Log.warn('Should Warn');
      Log.off();
    });
  });
});
