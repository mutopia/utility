define([
  'Setter',
  'Types'
], function(Setter, Types) {
  describe('Setter', function() {

    it('can require values', function() {
      var empty = {};
      expect(function() {
        Setter.require(empty, 'something');
      });

      var notEmpty = {
        something: true
      };
      expect(Setter.require(notEmpty, 'something')).toBe(true);
    });

    it('can set default value', function() {
      expect(Setter.def(undefined, 1)).toEqual(1);
      expect(Setter.def(0, 1)).toEqual(0);
    });

    it('can set default value based on a constructor', function() {
      var cstr = function (arg1, arg2) {
        var a = arg1 || 1;
        var b = arg2 || 2;
        this.output = a + b;
      };

      var input = null;
      // Check for default construction.
      expect(Setter.defCstr(input, cstr).output).toEqual(3);
      // Check for default construction with multiple args
      expect(Setter.defCstr(input, cstr, 3).output).toEqual(5);
      // Check for non-default construction.
      input = 7; // arg2 = 2 by default.
      expect(Setter.defCstr(input, cstr).output).toEqual(9);
    });

    it('can get max value', function() {
      expect(Setter.max(1, 2)).toEqual(2);
      expect(Setter.max(2, 1)).toEqual(2);
    });

    it('can get min value', function() {
      expect(Setter.min(1, 2)).toEqual(1);
      expect(Setter.min(2, 1)).toEqual(1);
    });

    it('can get value bounded by range', function() {
      expect(Setter.range(1, 2, 3)).toEqual(2);
      expect(Setter.range(4, 2, 3)).toEqual(3);
    });

    it('can deep merge objects', function() {
      expect(Setter.merge(null, null)).toEqual(null);
      expect(Setter.merge({}, null)).toEqual({});
      expect(Setter.merge(1, null)).toEqual(1);
      expect(Setter.merge(
          {a: {b: 123}},
          {a: {c: 456}}
      )).toEqual({a: {b: 123, c: 456}});
      expect(Setter.merge(
          {a: {b: [
            {c: 123}
          ]}},
          {a: {b: [
            {d: 456},
            {e: 789}
          ]}}
      )).toEqual(
          {a: {b: [
            {c: 123, d: 456},
            {e: 789}
          ]}}
      );
    });

    it('can shallow merge objects', function() {
      expect(Setter.mixin(null, null)).toEqual(null);
      expect(Setter.mixin({}, null)).toEqual({});
      expect(Setter.mixin(1, null)).toEqual(1);
      expect(Setter.mixin(
          {a: {b: 123}},
          {a: {c: 456}}
      )).toEqual({a: {c: 456}});
    });

    it('can shallow clone objects', function() {
      var objA = {a: 1, d: {e: 1}};
      var objB = Setter.clone(objA);
      objA.b = 2;
      objA.d.f = 4;
      objB.c = 3;
      expect(objA).toEqual({a: 1, b: 2, d: {e: 1, f: 4}});
      expect(objB).toEqual({a: 1, c: 3, d: {e: 1, f: 4}});
    });

    it('can deep clone objects', function() {
      var objA = {a: {b: 1}};
      var objB = Setter.cloneDeep(objA);
      objA.a.c = 2;
      objB.a.d = 3;
      expect(objA).toEqual({a: {b: 1, c: 2}});
      expect(objB).toEqual({a: {b: 1, d: 3}});
    });

    it('can clone arrays', function() {
      var objA = [1, 2, 3];
      var objB = Setter.clone(objA);
      expect(objA === objB).toBe(false);
      expect(objA.length).toEqual(objB.length);
      expect(Types.isArrayLiteral(objB)).toBe(true);
      expect(objA).toEqual(objB);
    });

    it('can deep clone arrays', function() {
      var objA = [{a: {b: 1}}];
      var objB = Setter.cloneDeep(objA);
      expect(objA[0] === objB[0]).toBe(false);
    });

  });
});
