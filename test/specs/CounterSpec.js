define([
  'Counter'
], function(Counter) {
  describe('Counter', function() {

    it('can construct with zero count', function() {
      expect(new Counter().getCount()).toEqual(0);
    });

    it('can construct with preset count', function() {
      expect(new Counter({count: 1}).getCount()).toEqual(1);
      expect(new Counter(2).getCount()).toEqual(2);
    });

    it('can increment', function() {
      var counter = new Counter({count: 1});
      expect(counter.increment()).toEqual(2);
      expect(counter.increment()).toEqual(3);
    });

    it('can decrement', function() {
      var counter = new Counter({count: 1});
      expect(counter.decrement()).toEqual(0);
      expect(counter.decrement()).toEqual(-1);
    });

  });
});
