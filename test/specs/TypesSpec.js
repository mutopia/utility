define([
  'Types'
], function(Types) {
  describe('Types', function() {

    it('can get type', function() {
      expect(Types.getTypeOf(undefined)).toEqual('Undefined');
      expect(Types.getTypeOf(null)).toEqual('Null');
      expect(Types.getTypeOf('foo')).toEqual('String');
      expect(Types.getTypeOf(1)).toEqual('Number');
      expect(Types.getTypeOf({})).toEqual('Object');
      expect(Types.getTypeOf([])).toEqual('Array');
    });

    it('can check type', function() {
      expect(Types.isType('', 'String')).toBe(true);
    });

    it('can check object literal', function() {
      expect(Types.isObjectLiteral({})).toBe(true);
      expect(Types.isObjectLiteral(new Function())).toBe(false);
      expect(Types.isObjectLiteral(1)).toBe(false);
      expect(Types.isObjectLiteral(null)).toBe(false);
      expect(Types.isObjectLiteral([])).toBe(false);
    });

    it('can check object', function() {
      expect(Types.isObject({})).toBe(true);
      expect(Types.isObject(new Function())).toBe(true);
      expect(Types.isObject(1)).toBe(false);
      expect(Types.isObject(null)).toBe(false);
      expect(Types.isObject([])).toBe(true);
    });

    it('can check empty object', function() {
      expect(Types.isEmptyObject({})).toBe(true);
      expect(Types.isEmptyObject({a: 123})).toBe(false);
      expect(Types.isEmptyObject(new Function())).toBe(false);
      expect(Types.isEmptyObject(1)).toBe(false);
      expect(Types.isEmptyObject(null)).toBe(false);
    });

    it('can check array literal', function() {
      expect(Types.isArrayLiteral([])).toBe(true);
      expect(Types.isArrayLiteral({length: 0})).toBe(false);
      expect(Types.isArrayLiteral(1)).toBe(false);
      expect(Types.isArrayLiteral(null)).toBe(false);
    });

    it('can check function literal', function() {
      expect(Types.isFunctionLiteral(new Function())).toBe(true);
      expect(Types.isFunctionLiteral({})).toBe(false);
      expect(Types.isFunctionLiteral(null)).toBe(false);
    });

    it('can check function', function() {
      expect(Types.isFunction(new Function())).toBe(true);
      expect(Types.isFunction(Object)).toBe(true);
      expect(Types.isFunction(null)).toBe(false);
    });

    it('can check string', function() {
      expect(Types.isString('foo')).toBe(true);
      expect(Types.isString({})).toBe(false);
      expect(Types.isString(null)).toBe(false);
    });

    it('can check boolean', function() {
      expect(Types.isBoolean(true)).toBe(true);
      expect(Types.isBoolean(false)).toBe(true);
      expect(Types.isBoolean({})).toBe(false);
      expect(Types.isBoolean(null)).toBe(false);
    });

    it('can check number', function() {
      expect(Types.isNumber(0)).toBe(true);
      expect(Types.isNumber(-1)).toBe(true);
      expect(Types.isNumber(1)).toBe(true);
      expect(Types.isNumber('1')).toBe(false);
      expect(Types.isNumber({})).toBe(false);
      expect(Types.isNumber(null)).toBe(false);
    });

    it('can check null or undefined', function() {
      expect(Types.isNullOrUndefined(null)).toBe(true);
      expect(Types.isNullOrUndefined(undefined)).toBe(true);
      expect(Types.isNullOrUndefined(0)).toBe(false);
      expect(Types.isNullOrUndefined('')).toBe(false);
    });

    it('can check primitive', function() {
      expect(Types.isPrimitive(null)).toBe(true);
      expect(Types.isPrimitive(undefined)).toBe(true);
      expect(Types.isPrimitive(0)).toBe(true);
      expect(Types.isPrimitive('')).toBe(true);
      expect(Types.isPrimitive({})).toBe(false);
      expect(Types.isPrimitive([])).toBe(false);
      expect(Types.isPrimitive(new Function())).toBe(false);
    });

  });
});
