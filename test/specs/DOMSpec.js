define([
  'DOM'
], function(DOM) {
  describe('DOM', function() {

    it('can check nodes', function() {
      var node = document.createElement('div');
      expect(DOM.isNode(node)).toBe(true);
      expect(DOM.isNode({})).toBe(false);
    });

  });
});
