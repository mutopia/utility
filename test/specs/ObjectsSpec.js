define([
  'Objects'
], function(Objects) {
  describe('Objects', function() {

    it('can get values', function() {
      expect(Objects.values({})).toEqual([]);
      expect(Objects.values({foo: 'bar', abc: 123})).toEqual(['bar', 123]);
    });

  });
});
