define([
  'Strings'
], function(Strings) {
  describe('Strings', function() {

    it('can convert to title case', function() {
      expect(Strings.toTitleCase('foo bar')).toEqual('Foo Bar');
    });

    it('can convert camel case to hyphens', function() {
      expect(Strings.camelToHypthen('fooBar')).toEqual('foo-bar');
    });

    it('can convert camel case to title case', function() {
      expect(Strings.camelToTitleCase('FooBar')).toEqual('Foo Bar');
    });

    it('can splice', function() {
      expect(Strings.splice('abcdef', 2, 3, 'foo')).toEqual('abfoodef');
    });

    // TODO(aramk) replaceRegex

    it('can trim objects', function() {
      expect(Strings.trimObj({a: ' b '})).toEqual({a: 'b'});
    });

    it('can repeat', function() {
      expect(Strings.repeat('banana', 3)).toEqual('bananabananabanana');
    });

    it('can escape', function() {
      expect(Strings.escape('<tag>')).toEqual('&lt;tag&gt;');
    });

    it('can pluralize', function() {
      expect(Strings.pluralize('dog', 4, 'dogs')).toEqual('dogs');
      expect(Strings.pluralize('dog', 1, 'dogs')).toEqual('dog');
    });

    it('can linkify html', function() {
      expect(Strings.linkifyHtml('foo bar')).toEqual('foo bar');
      expect(Strings.linkifyHtml('foo https://test.com/page/123 bar'))
          .toEqual('foo <a href="https://test.com/page/123">https://test.com/page/123</a> bar');
      expect(Strings.linkifyHtml('foo https://test.com/page/123 bar', {target: '_blank'}))
          .toEqual('foo <a href="https://test.com/page/123" target="_blank">https://test.com/page/123</a> bar');
    });

  });
});
