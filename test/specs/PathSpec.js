define([
  'Path'
], function(Path) {
  describe('Path', function() {

    it('can get directory name', function() {
      expect(Path.dirname('a/b/c')).toEqual('a/b');
    });

    it('can get directory name', function() {
      expect(Path.basename('a/b/c.txt')).toEqual('c.txt');
      expect(Path.basename('a/b/c/d')).toEqual('d');
      expect(Path.basename('a')).toEqual('a');
    });

    it('can get filename', function() {
      expect(Path.filename('a/b/c.txt')).toEqual('c');
    });

    it('can get extension', function() {
      expect(Path.extension('a/b/c.txt')).toEqual('txt');
    });

    it('can check if relative', function() {
      expect(Path.isRelative('a/b/c.txt')).toBe(true);
      expect(Path.isRelative('./a/b/c.txt')).toBe(true);
      expect(Path.isRelative('/a/b/c.txt')).toBe(false);
    });

    it('can add slash', function() {
      expect(Path.addLastSlash('a/b/c')).toEqual('a/b/c/');
      expect(Path.addLastSlash('a/b/c/')).toEqual('a/b/c/');
      expect(Path.addLastSlash('')).toEqual('/');
    });

    it('can remove slash', function() {
      expect(Path.removeLastSlash('a/b/c')).toEqual('a/b/c');
      expect(Path.removeLastSlash('a/b/c/')).toEqual('a/b/c');
      expect(Path.removeLastSlash('/')).toEqual('');
      expect(Path.removeLastSlash('')).toEqual('');
    });

    it('can remove query', function() {
      expect(Path.removeQuery('test?foo=bar')).toEqual('test');
      expect(Path.removeQuery('test')).toEqual('test');
    });

  });
});
