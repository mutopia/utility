// Find all files loaded by karma based on "files" ending in Spec and load them as tests with
// RequireJS.
var tests = [];
for (var file in window.__karma__.files) {
  if (window.__karma__.files.hasOwnProperty(file)) {
    if (/Spec\.js$/.test(file)) {
      tests.push(file);
    }
  }
}

requirejs.config({
  // Karma serves files from '/base'.
  baseUrl: '/base/src',

  packages: [
  ],

  // Ask requirejs to load these files.
  deps: tests,

  // Start tests running once requirejs is done.
  callback: function() {
    require(['Log'], function(Log) {
      Log.setLevel('debug');
      Log.off();
      window.__karma__.start();
    });
  }
});
