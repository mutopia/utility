define([], function() {

  // summary:
  //      Window utility methods.

  return {
    addSlash: function(str) {
      return str.replace(/(?!\/)(.)$/, '$1/');
    },
    getQueryParams: function(qs) {
      // http://stackoverflow.com/questions/439463
      qs = qs.split("+").join(" ");
      var params = {}, tokens, re = /[?&]?([^=]+)=([^&]*)/g;
      while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
      }
      return params;
    },
    currentURL: function(_window) {
      _window = _window || window;
      return _window.location.protocol + '//' + _window.location.host + _window.location.pathname;
    },
    currentHost: function(_window) {
      _window = _window || window;
      return this.addSlash(_window.location.protocol + '//' + _window.location.host);
    },
    currentDir: function() {
      return this.addSlash(this.currentURL().substring(0, this.currentURL().lastIndexOf('/')));
    },
    GET: function(key, _window) {
      _window = _window || window;
      var GET = this.getQueryParams(_window.location.search);
      if (key) {
        return GET[key];
      } else {
        return GET;
      }
    },
    isEnabled: function(name) {
      var setting = this.GET(name);
      return setting !== undefined ? setting !== "0" : true;
    },
    // Obsolete
    getVar: function(name) {
      var GET = this.GET();
      return GET[name];
    },
    getVarBool: function(name) {
      var variable = this.GET(name);
      if (typeof variable == 'undefined') {
        return undefined;
      } else {
        return !!parseInt(variable);
      }
    },
    isPhantomJs: function() {
      return navigator.userAgent.indexOf('PhantomJS') >= 0;
    }
  };

});
