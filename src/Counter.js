define([
  './Class',
  './Setter',
  './Types'
], function(Class, Setter, Types) {
  /**
   * Keeps a numerical count and allows incrementing.
   */
  return Class.extend({

    _count: null,
    _startCount: null,

    _init: function(args) {
      if (Types.isNumber(args)) {
        args = {count: args};
      }
      args = Setter.merge({
        count: 0
      }, args);
      this._count = this._startCount = args.count;
    },

    getCount: function() {
      return this._count;
    },

    setCount: function(count) {
      this._count = count;
    },

    increment: function() {
      return ++this._count;
    },

    decrement: function() {
      return --this._count;
    },

    reset: function() {
      this._count = this._startCount;
    }

  });
});
