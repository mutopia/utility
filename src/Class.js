/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 * Inspired by base2 and Prototype
 *
 * Changelog:
 *   Modified into an AMD module.
 *   init() renamed to _init().
 *   Added some comments and cleaned up names.
 *   Class.extend(...) can extend other classes directly.
 *   Class.extend([...]) shortcut to form an inheritance chain
 */
define([
  './Log',
  './Types'
], function(Log, Types) {
  var initializing = false;

  // Test if you can regex within functions, and then return an appropriate regex to match on
  // functions that have been overridden.
  // If you can, return a regex for "_super". If a function uses _super; a reference to the
  // overridden function needs to be retained.
  // If you cant, return a regex which will match anything and therefore store references
  // to all functions as a fail safe.
  var usesSuperTest = /xyz/.test(function() {
    xyz; // jshint ignore: line
  }) ? /\b_super\b/ : /.*/;

  // The base Class implementation (does nothing)
  var Class = function() {};

  /**
   * Overrides the named function of the super class with the same named function on the subclass,
   * whilst retaining a reference to the superclass version which can be called from within the
   * subclass version.
   * Note: This is an instanced method and should be correctly bound to 'this'.
   *
   * @param {Object} superClassProto - The super class.
   * @param {String} name - The name of the method being overridden.
   * @param {Function} fn - The subclass function that is overriding.
   * @returns {Function} The wrapper function to assign to the subclass for the named method.
   */
  function overrideSuperFunction(superClassProto, name, fn) {
    return function() {
      var tmp = this._super;

      // Add a new ._super() method that is the same method
      // but on the super-class
      this._super = superClassProto[name];

      // The method only need to be bound temporarily, so we
      // remove it when we're done executing
      var ret = fn.apply(this, arguments);
      this._super = tmp;

      return ret;
    };
  }

  /**
   * Extends the calling <code>Class</code> with the given Classes, and then extending that class
   * hierarchy with the given prototype.
   *
   * The first Class in the <code>classHierarchy</code> array forms the "base" of the class
   * hierarchy (after the calling Class), and the last element is the second to last subclass of
   * the class hierarchy. Finally, the given prototype object extends the class hierarchy.
   *
   * Note: The intermediary classes in <code>classHierarchy</code> are purely mixin classes, and
   * will not be resolved with calls to <code>instanceof</code>.
   *
   * @param {Array.<Class>} [classHierarchy=Null] - The class hierarchy to extend on top of.
   * @param {Object} prototype - Prototype of the Class to build on top of the defined class
   *     hierarchy. If only one argument is provided, it is assumed to be the <code>prototype</code>
   *     argument.
   * @returns {Class} The constructed Class.
   */
  Class.extend = function extendFunction(classHierarchy, prototype) {
    var subClass = null;

    // Check whether the correct arguments have been provided.
    if ((!prototype && !Types.isObjectLiteral(classHierarchy)) &&
        ((!classHierarchy || Types.isArrayLiteral(classHierarchy)) &&
            (!Types.isObjectLiteral(prototype)))) {
      throw new Error('Tried to extend a class without specifying subclass.');
    }

    // If 'prototype' is not given, 'classHierarchy' must be the prototype.
    if (!prototype) {
      prototype = classHierarchy;
      classHierarchy = null;
    }

    // Check whether this is a simple extension. If it is, the calling Class is extended with
    // prototype and nothing further needs to occur.
    if (!classHierarchy || classHierarchy.length === 0) {
      subClass = this._extend(prototype);
    } else {

      // Only option left now is constructing a more complicated class hierarchy.

      // Extract first subclass to use as the base class of the class hierarchy.
      var baseClass = classHierarchy.shift();

      // Construct the class hierarchy, starting with baseClass.
      subClass = classHierarchy.reduce(function(superClass, subClass) {
        // Check if a malformed Class was accidentally mixed into the class hierarchy.
        if (!superClass || !superClass._extend) {
          throw new Error('Tried to extend off a null object.');
        }

        return superClass._extend(subClass);
      }, baseClass);

      // Now the class hierarchy extend with the given prototype.
      subClass.extend = extendFunction;
      subClass = subClass._extend(prototype);
    }

    if (!subClass) {throw new Error('Error constructing subclass');}
    subClass.extend = extendFunction;
    return subClass;
  };

  /**
   * Extends the calling <code>Class</code> with the given Class or class prototype, constructing
   * a new <code>Class</code>
   *
   * @param {Class|Object} subClassProto - The extending class.
   * @returns {Class} The extended class.
   */
  Class._extend = function _extendFunction(subClassProto) {
    if (subClassProto === {}) {
      throw new Error('This should be tested');
    }
    var _superClassProto = this.prototype;

    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var newPrototype = new this();
    initializing = false;

    // Check whether an existing Class subClassProto is being extended.
    if (subClassProto.extend) {
      subClassProto = subClassProto.prototype;
    }

    // Copy the properties to the new prototype.
    Object.getOwnPropertyNames(subClassProto).forEach(function(name) {

      var subFunc = subClassProto[name];
      var superFunc = _superClassProto[name];

      // Check if we're overwriting an existing function
      if (Types.isFunction(subFunc) && Types.isFunction(superFunc) && usesSuperTest.test(subFunc)) {
        newPrototype[name] = overrideSuperFunction.bind(this)(_superClassProto, name, subFunc);
      } else {
        newPrototype[name] = subClassProto[name];
      }
    });

    // The dummy class constructor
    function Class() {
      // All construction is actually done in the _init method
      if (!initializing && this._init) {
        this._init.apply(this, arguments);
      }
    }

    // Populate our constructed newPrototype object
    Class.prototype = newPrototype;

    // Enforce the constructor to be what we expect
    Class.prototype.constructor = Class;

    // And make this class extendible
    Class._extend = _extendFunction;

    return Class;
  };

  return Class;
});
