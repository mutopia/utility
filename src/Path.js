define([
  './Setter',
  './Log'
], function(Setter, Log) {

  /**
   * Utility functions for playing with paths.
   */
  return {

    /**
     * @param {String} path
     * @param {Number} [times=1] - The number of executions on the given path.
     * @returns {String} The given path with the last segment removed.
     */
    dirname: function(path, times) {
      times = Setter.def(times, 1);
      path = this._clean(path);
      return this._repeatExec(function (path) {
        return path.replace(/\/[^/]*$/, '');
      }, path, times);
    },

    /**
     * @param {String} path
     * @param {Number} [times=1] - The number of executions on the given path.
     * @returns {String} The last segment of the path.
     */
    basename: function(path, times) {
      times = Setter.def(times, 1);
      return this._repeatExec(function (path) {
        return this._firstMatch(path, /[^/]*$/);
      }.bind(this), path, times);
    },

    /**
     * @param {String} path
     * @returns {String} The last segment of the path with any extensions removed.
     */
    filename: function(path) {
      return this.basename(path).replace('.' + this.extension(path, true), '');
    },

    /**
     * @param {String} path
     * @returns {String} The extension of a the last segment in the path.
     */
    extension: function(path) {
      var ext = this._firstMatch(path, /\.[^./]*$/);
      return ext ? ext.replace('.', '') : ext;
    },

    /**
     * @param {...String} paths - A set of paths to concatenate, separated by forward-slashes.
     */
    join: function() {
      var path = '';
      for (var i = 0; i < arguments.length; i++) {
        path = this.addLastSlash(path + arguments[i]);
      }
      return path;
    },

    /**
     * @param {String} path
     * @returns {Boolean} Whether the given path is relative.
     */
    isRelative: function(path) {
      return /^(?!\/)[\s\S]/.test(path);
    },

    _firstMatch: function(str, regex) {
      str = this._clean(str);
      var match = str.match(regex);
      return match && match.length > 0 ? match[0] : null;
    },

    /**
     * @param {Function} callback
     * @param arg - The single argument to call the callback with. The callback should return a
     * similar object.
     * @param {Number} times - The number of times to execute the callback.
     * @returns {*} The result from calling the given callback with the given callback and chaining the
     * output the given number of times.
     * @private
     */
    _repeatExec: function(callback, arg, times) {
      for (var i = 0; i < times; i++) {
        arg = callback(arg);
      }
      return arg;
    },

    _clean: function(obj) {
      if (!obj) {
        obj = '';
      }
      if (!(typeof obj == 'string')) {
        Log.error('Needs a string, given', obj);
      }
      return obj;
    },

    /**
     * @param {String} path
     * @returns {String} Adds a trailing forward-slash to the path if not present.
     */
    addLastSlash: function(path) {
      var last = path.substr(path.length - 1);
      return last == '/' ? path : path + '/';
    },

    /**
     * @param {String} path
     * @returns {String} Removes any trailing forward-slash in the path if present.
     */
    removeLastSlash: function(path) {
      return path.replace(/\/$/, '');
    },

    /**
     * @param {String} url
     * @returns {String} Removes any query string following a "?" (inclusive).
     */
    removeQuery: function(url) {
      return url.replace(/\?.*/, '');
    }

  };

});
