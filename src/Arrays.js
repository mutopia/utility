define([], function() {
  /**
   * Array utility methods.
   */
  return {
    first: function(array) {
      return array[0];
    },

    insert: function(array, index, obj) {
      return array.splice(index, 0, obj);
    },

    last: function(array) {
      return array[array.length - 1];
    },

    equals: function(array, other) {
      return other instanceof Array && JSON.stringify(array) === JSON.stringify(other);
    },

    /**
     * Transforms a set of arguments into a single array of arguments.
     * @returns {Array}
     */
    argsArray: function() {
      var args = [];
      $.each(arguments, function(i, arg) {
        if (arg instanceof Array) {
          $.each(arg, function(j, item) {
            args.push(item);
          });
        } else {
          args.push(arg);
        }
      });
      return args;
    },

    difference: function(a, b) {
      return a.filter(function(item) {
        return b.indexOf(item) === -1;
      });
    },

    clone: function (array) {
      return Array.prototype.slice.apply(array);
    }

  }
});
