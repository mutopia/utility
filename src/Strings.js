define([
  './Setter'
], function(Setter) {
  /**
   * String utility methods.
   */
  return {

    /**
     * @returns The string converted to title case.}
     */
    toTitleCase: function(str) {
      var parts = str.split(/\s+/);
      var title = '';
      for (var i = 0; i < parts.length; i++) {
        var part = parts[i];
        if (part != '') {
          title += part.slice(0, 1).toUpperCase() + part.slice(1, part.length);
          if (i != parts.length - 1 && parts[i + 1] != '') {
            title += ' ';
          }
        }
      }
      return title;
    },

    camelToHypthen: function(str) {
      var parts = str.split(/(?=[A-Z])/);
      var hyphen = '';
      for (var i = 0; i < parts.length; i++) {
        var part = parts[i];
        hyphen += part.toLowerCase();
        if (i < parts.length - 1) {
          hyphen += '-';
        }
      }
      return hyphen;
    },

    camelToTitleCase: function(str) {
      var re = /([A-Z]+(?![a-z]))|([A-Z][a-z]+)/g, match;
      var title = [];
      while ((match = re.exec(str)) !== null) {
        title.push(match[0]);
      }
      return title.join(' ');
    },

    splice: function(str, i, j, repl) {
      // Works like Array.splice().
      // TODO(aramk) j should be how many, not index.
      return str.substr(0, i) + repl + str.substr(j, str.length);
    },

    /**
     * Similar to String.replace(regex, callback) but the callback is called with the
     * result of the regex.exec(str)
     */
    replaceRegex: function(str, regex, callback) {
      var match, copy = str + '';
      while (match = regex.exec(copy)) {
        var repl = callback(match);
        copy = this.splice(copy, match.index, match.index + match[0].length, repl);
      }
      return copy;
    },

    /**
     * @returns The given object with the string values trimmed in-place.
     */
    trimObj: function(obj) {
      var result = {};
      for (var key in obj) {
        var val = obj[key];
        if (typeof val == 'string') {
          val = val.trim();
        }
        result[key] = val;
      }
      return result;
    },

    repeat: function(str, n) {
      return (new Array(n + 1)).join(str);
    },

    escape: function(str) {
      var entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;'
      };
      return str.replace(/[&<>"'\/]/g, function(s) {
        return entityMap[s];
      });
    },

    /**
     * @param {String} singular - A noun in singular form.
     * @param {Number} [count=0] - The number of items for this noun.
     * @param {String} [plural] - The noun in plural form. If not specified, it is guessed.
     * @returns {String} The correct form (singular or plural) based on the given count. If no count
     * is given, the plural form is returned.
     */
    pluralize: function(singular, count, plural) {
      count = Setter.def(count, 0);
      // TODO(aramk) Improve automatic pluralization.
      plural = Setter.def(plural, singular + 's');
      return count == 1 ? singular : plural;
    },

    /**
     * @param {String} html - An HTML string.
     * @param {Object} [args]
     * @param {String} [args.target]
     * @return {String} The given HTML string with all links wrapped in anchor tags.
     */
    linkifyHtml: function(html, args) {
      var atts = '';
      if (args && args.target) {
        atts = ' target="' + args.target + '"';
      }
      return html.replace(/(\w+?:\/\/[^\s]+)/gmi, '<a href="$1"' + atts + '>$1</a>');
    }

  };
});
