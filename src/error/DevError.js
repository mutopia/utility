define([
  './AbstractError'
], function(AbstractError) {
  /**
   * A generic error.
   * @class utility.error.DevError
   * @param {String} message
   * @param [...] args - Arbitrary arguments.
   * @abstract
   */
  return AbstractError.extend({

    name: 'DevError'

  });
});
