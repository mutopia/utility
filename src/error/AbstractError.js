define([
  '../Class',
  '../Log'
], function(Class, Log) {
  /**
   * A generic error.
   * @class utility.error.AbstractError
   * @abstract
   */
  return Class.extend({

    name: 'AbstractError',

    /**
     * @param {String} message
     * @param [...] Arbitrary arguments associated with the error.
     */
    _init: function(message) {
      this.message = message;
      // The stack is built on instantiation, not on the throws call.
      this.stack = (new Error()).stack;
      this.args = Array.prototype.slice.call(arguments, 1);
    },

    /**
     * @param {Boolean} log - Whether an error should be logged with {@link Log} to print out the
     * additional arguments (if they exist).
     * @returns {string} A string of the stack trace.
     */
    toString: function(log) {
      if (log !== false) {
        var errorArgs = [this.name, this.message].concat(this.args);
        Log.error.apply(Log, errorArgs);
      }
      var str = this.name + ': ' + this.message;
      if (this.stack) {
        str += '\n' + this.stack.toString();
      }
      return str;
    },

    /**
     * Logs the stack trace.
     */
    logStack: function() {
      Log.error(this.stack.toString());
    }

  });
});
