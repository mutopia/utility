define([
    // TODO(aramk) Remove dependence on Dojo.
  'dojo/_base/declare',
  'dojo/date/locale'
], function(declare, locale) {

  // summary:
  //      Date utility functions.

  // The singleton instance
  var instance;

  var DateUtil = declare(null, {
    dateFormat: 'yyyy/MM/d',
    dateShortFormat: 'MMMM d',
    timeFormat: 'h:m:s a',
    dateTimeFormat: 'yyyy/MM/d - h:m:s a',
    formatDate: function(date) {
      return locale.format(new Date(date), { selector: 'date', datePattern: this.dateFormat });
    },
    formatLong: function(date) {
      return locale.format(new Date(date), { selector: 'date', formatLength: 'long' });
    },
    formatDateInterval: function(before, after) {
      var beforeDate = before ? new Date(before) : null;
      var afterDate = after ? new Date(after) : null;
      var beforePattern = 'MMMM d';
      var afterPattern = beforePattern + ', yyyy';
      if (beforeDate && afterDate && beforeDate.getYear() != afterDate.getYear()) {
        beforePattern = afterPattern;
      }
      var date = '';
      if (beforeDate) {
        date += locale.format(beforeDate, { selector: 'date', datePattern: beforePattern });
      }
      if (afterDate) {
        if (date != '') {
          date += ' - ';
        }
        date += locale.format(afterDate, { selector: 'date', datePattern: afterPattern });
      }
      return date;
    },
    formatTime: function(date) {
      return locale.format(new Date(date), { selector: 'date', datePattern: this.timeFormat });
    },
    formatDateTime: function(date) {
      return locale.format(new Date(date), { selector: 'date', datePattern: this.dateTimeFormat });
    }
  });
  // summary:
  //      Return the instance retrieval method for others to use.
  return (instance = (instance || new DateUtil()));
});
