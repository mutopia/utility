define([], function() {
  return {

    /**
     * @returns {String} The name of the prototype for the given object.
     */
    getTypeOf: function(object) {
      return Object.prototype.toString.call(object).slice(8, -1);
    },

    /**
     * @param object
     * @param {String} type
     * @returns {Boolean} Whether the given object is of the given type.
     * @private
     */
    isType: function(object, type) {
      return this.getTypeOf(object) === type;
    },

    /**
     * @returns {Boolean} Whether the given object is an object literal (has the prototype
     * {@link Object}).
     */
    isObjectLiteral: function(object) {
      return object != null && typeof object === 'object' && !this.isFunction(object) &&
          !this.isArrayLiteral(object);
    },

    /**
     * @returns {Boolean} Whether the given object is either an object literal, an object
     * instantiated from a function or an array.
     */
    isObject: function(object) {
      return (object != null && typeof object === 'object') || this.isFunction(object) ||
          this.isArrayLiteral(object);
    },

    /**
     * @param {*} obj
     * @returns {Boolean} Whether the given object is an empty object literal.
     */
    isEmptyObject: function(obj) {
      return this.isObjectLiteral(obj) && Object.keys(obj).length === 0;
    },

    /**
     * @returns {Boolean} Whether the given object is an array literal (has the prototype
     * {@link Array}).
     */
    isArrayLiteral: function(object) {
      if (Array.isArray) {
        return Array.isArray(object);
      } else {
        return this.isType(object, 'Array');
      }
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is a function literal (not a function with
     * a custom prototype).
     */
    isFunctionLiteral: function(object) {
      return this.isType(object, 'Function');
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is any kind of function (e.g. literal,
     * constructor with a custom prototype).
     */
    isFunction: function(object) {
      return typeof object === 'function';
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is a String.
     */
    isString: function(object) {
      return typeof object === 'string';
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is a Boolean.
     */
    isBoolean: function(object) {
      return typeof object === 'boolean';
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is a Number.
     */
    isNumber: function(object) {
      return typeof object === 'number' && !isNaN(object);
    },

    /**
     * @param object
     * @returns {Boolean} Whether the given object is a floating-point Number.
     */
    isFloat: function(object) {
      return this.isNumber(object) && object.toString().indexOf('.') >= 0;
    },

    /**
     * @returns {boolean} Whether the give object is null or undefined.
     */
    isNullOrUndefined: function(object) {
      return object === null || typeof object === 'undefined';
    },

    isPrimitive: function(object) {
      return !this.isFunction(object) && !this.isObjectLiteral(object) &&
          !this.isArrayLiteral(object);
    }

  };
});
