define([
  './Arrays',
  './Class',
  './Counter',
  './DateUtil',
  './DOM',
  './Log',
  './Numbers',
  './Objects',
  './Path',
  './Setter',
  './Strings',
  './Types',
  './Window',
  './error/AbstractError',
  './error/DevError'
], function() {
  /**
   * This is used in production as a means of forcing the Dojo build system to combine all modules
   * into a single layer. Update the imports above to include them in the core layer.
   */
});
