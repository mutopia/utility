define([], function() {
  return {

    /**
     * @param {Object} obj
     * @param {Function} [callback] - Invoked on each object. The return value is added to the
     * returned array.
     * @param {Object} [scope] - The scope used to invoke the callback (if given).
     * @returns {Array} The values from the given object.
     */
    values: function(obj, callback, scope) {
      scope = scope || this;
      return Object.getOwnPropertyNames(obj).map(function(key) {
        var value = obj[key];
        return callback ? callback.call(scope, value) : value;
      });
    }

  }
});
