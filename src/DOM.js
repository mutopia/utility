define([], function() {
  return {

    isNode: function(o) {
      if (typeof Node === 'object') {
        return o instanceof Node;
      } else {
        return o && typeof o === 'object' && typeof o.nodeType === 'number' &&
            typeof o.nodeName === 'string';
      }
    }

  };
});
