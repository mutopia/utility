var global = this;
var console = global.console;

define([
  './Setter',
  './Window'
], function(Setter, Window) {

  // summary:
  //      Logging utility functions.

  // For those without a console
  if (console === undefined) {
    console = {};
    var funcs = ['log', 'debug', 'error', 'warn', 'time', 'timeEnd'];
    for (var i = 0; i < funcs.length; i++) {
      var func = funcs[i];
      console[func] = function() {
        // Ignorance is bliss
      };
    }
  }

  var Log = {
    // level:
    //    The current level of logging.
    level: 'info',

    _levels: {
      off: 0,
      error: 1,
      warn: 2,
      info: 3,
      time: 4,
      debug: 5
    },

    _timers: {},

    setLevel: function(level) {
      if (level in this._levels) {
        this.level = level;
      }
    },

    _loggingOn: true,

    _shouldLog: function(level) {
      // Only logs if the level is less than or equal to the current level
      var code = this._levels[level];
      return code !== undefined && code <= this._levels[this.level];
    },

    on: function() {
      if (this._loggingOn) {
        return;
      }
      this._loggingOn = true;

      Object.keys(_functionReferences).forEach(function(f) {
        this[f] = _functionReferences[f];
      }, this);
    },

    off: function() {
      if (!this._loggingOn) {
        return;
      }
      this._loggingOn = false;

      Object.keys(_functionReferences).forEach(function(f) {
        this[f] = function() {};
      }, this);
    },

    debug: function() {
      if (this._shouldLog('debug')) {
        Log.msg('DEBUG', arguments, console.debug);
      }
    },

    info: function() {
      if (this._shouldLog('info')) {
        Log.msg('INFO', arguments);
      }
    },

    warn: function() {
      if (this._shouldLog('warn')) {
        Log.msg('WARNING', arguments, console.warn);
      }
    },

    error: function() {
      if (this._shouldLog('error')) {
        Log.msg('ERROR', arguments, console.error);
      }
    },

    msg: function(msg, args, func) {
      if (this.level !== 'off') {
        func = Setter.def(func, console.log);
        args = Array.prototype.slice.call(args);
        args.splice(0, 0, '[' + msg + '] ');
        func.apply(console, args);
      }
    },

    trace: function() {
      var e = new Error('dummy');
      var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
          .replace(/^\s+at\s+/gm, '')
          .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
          .split('\n');
      console.log(stack);
    },

    time: function(name, level) {
      this._timers[name] = {
        date: Date.now(),
        level: level
      };
      if (level == null && this._shouldLog('time')) {
        console.time(name);
      }
    },

    timeEnd: function(name) {
      var timer = this._timers[name];
      if (!timer) return;
      var time = Date.now() - timer.date;
      if (this._shouldLog(timer.level)) {
        Log[timer.level](name + ': ' + time + 'ms');
      } else if (this._shouldLog('time')) {
        console.timeEnd(name);
      }
      delete this._timers[name];
      return time;
    }
  };

  // Allows setting the logging level via GET variable.
  if (typeof window !== 'undefined') {
    var logGet = Window.GET('log');
    if (logGet) {
      Log.setLevel(logGet);
    }
  }

  var _functionReferences = {
    'info': Log.info,
    'debug': Log.debug,
    'error': Log.error,
    'warn': Log.warn,
    'time': Log.time,
    'timeEnd': Log.timeEnd
  };

  return Log;

});
