define([
  './Log',
  './Types'
], function(Log, Types) {

  function log(leader, msg, onConsole) {
    if (leader) {
      msg = leader + ': ' + msg;
    }
    if (onConsole) {
      /* global console */
      console.log(msg);
    } else {
      Log.debug(msg);
    }
  }

  return {
    inspect: function(obj, leader, onConsole) {
      // Strings don't get logged correctly if you "Stringify" them.
      if (typeof obj !== 'string') {
        // Functions need to be 'toString'ed rather than 'stringify'ed.
        if (typeof obj === 'function') {
          obj = obj.toString();
        } else {
          obj = JSON.stringify(obj, null, 4);
        }
      }
      log(leader, obj, onConsole);
    },

    enumerate: function(obj) {
      var str = '';
      Object.keys(obj).forEach(function(key) {
        if (typeof obj[key] === 'function') {
          str += key + '(), ';
        } else {
          str += key + ', ';
        }
      });
      return Types.getTypeOf(obj) + ': ' + str.substr(0, str.length - 2);
    },

    protoInspect: function(obj, leader, onConsole) {
      var _p = obj;
      var indent = '';
      var logSomething = true;
      do {
        _p = _p.prototype || _p.__proto__;
        if (Object.keys(_p).length === 0) { break; }

        log(leader, indent + this.enumerate(_p), onConsole);
        indent += '  ';
        logSomething = false;
      } while (_p && _p !== Object.prototype);

      if (logSomething) {log(leader, 'no prototype', onConsole);}
    }
  };

});
