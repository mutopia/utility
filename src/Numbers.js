define([], function() {
  /**
   * Number utility methods.
   */
  return {

    isDefined: function(value) {
      return value !== '' && value != null && !isNaN(value);
    },

    parse: function(value) {
      return typeof value === 'number' ? value : parseFloat(value);
    }

  };
});
