define([
  './Types'
], function(Types) {
  /**
   * Utility functions for setting variables.
   */
  return {

    /**
     * Returns the value of the named property on obj, or throws an error if that value is
     * undefined or null on the given object.
     * @param {Object} obj - The object which should contain the named property. Assumed to
     *     be defined.
     * @param {String} name - The name of the required property.
     * @param {String} [class] - Name of class to report in error message.
     * @returns {Object} The requested property.
     */
    require: function(obj, name, cstr) {
      if (Types.isNullOrUndefined(obj[name])) {
        var msg = name + ' is required' + (cstr ? ' to construct ' + cstr : '.');
        throw new Error(msg);
      }
      return obj[name];
    },

    /**
     * @param value
     * @param defaultValue
     * @returns {*} The given variable if defined or the default value.
     */
    def: function(value, defaultValue) {
      return (typeof value !== 'undefined') ? value : defaultValue;
    },

    /**
     * @param value - Value to construct based on.
     * @param {Function} cstr - The constructor to call.
     * @param cstrArgs - Arguments to pass to the constructor.
     * @return A constructed object based on the input.
     */
    defCstr: function(value, cstr, cstrArg) {
      if (Types.isNullOrUndefined(value)) {
        return new cstr(cstrArg);
      } else {
        return new cstr(value);
      }
    },

    defLazy: function(v, /*Function*/ def) {
      // summary:
      //      Returns the given variable if defined or executes the default value callback.
      // description:
      //      Lazy evaluation version of setDefault
      if (def instanceof Function) {
        if (typeof v !== 'undefined') {
          return v;
        } else {
          return def();
        }
      } else {
        return this.def(v, def);
      }
    },

    /**
     * @param {Number} value
     * @param {Number} max
     * @returns {Number} The given value or max, whichever is larger.
     */
    max: function(value, max) {
      return Math.max(value, max);
    },

    /**
     * @param {Number} value
     * @param {Number} min
     * @returns {Number} The given value or max, whichever is smaller.
     */
    min: function(value, min) {
      return Math.min(value, min);
    },

    /**
     * @param {Number} value
     * @param {Number} min
     * @param {Number} max
     * @returns {Number} The given value if it falls within the given range, or either min or max if
     * it doesn't.
     */
    range: function(value, min, max) {
      if (value < min) {
        return min;
      } else if (value > max) {
        return max;
      } else {
        return value;
      }
    },

    /**
     * Merges the given source object into the given destination object recursively (deep merging).
     * @param {Object} dest
     * @param {Object} source
     * @returns {Object} The modified destination object. If no source is provided, the destination
     * is returned unchanged.
     */
    merge: function(dest, source) {
      // summary:
      //      Works just like lang.mixin but allows mixing arrays within objects.

      // This will NOT try to merge two objects which have prototypes defined
      // (e.g. are instances of dojo classes)
      if (Types.isEmptyObject(dest) || Types.isNullOrUndefined(dest) ||
          Types.isNullOrUndefined(source) || Types.isPrimitive(dest) || Types.isPrimitive(source)
      /*|| Type.isFunction(source) || Type.isFunction(dest)*/) {
        if (!Types.isNullOrUndefined(dest) && !Types.isNullOrUndefined(source)) {
          return source;
        } else if (Types.isNullOrUndefined(source) && dest) {
          return dest;
        } else {
          return source;
        }
      } else if (Types.isEmptyObject(source) || !source || source === dest) {
        return dest;
      }
      for (var i in source) {
        if (!(i in dest)) {
          dest[i] = source[i];
        } else if (Types.isArrayLiteral(source[i])) {
          if (Types.isArrayLiteral(dest[i])) {
            for (var j = 0; j < source[i].length; j++) {
              dest[i][j] = this.merge(dest[i][j], source[i][j]);
            }
          } else {
            dest[i] = source[i];
          }
        } else if (source[i] === null) {
          dest[i] = source[i];
        } else {
          dest[i] = this.merge(dest[i], source[i]);
        }
      }
      return dest;
    },

    /**
     * Mixes in the properties of the given source object into the given destination object
     * non-recursively (shallow merging).
     * @param dest
     * @param source
     * @returns {*}
     */
    mixin: function(dest, source) {
      // Adapted from dojo/lang
      var name, s, i, empty = {};
      for (name in source) {
        // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
        // inherited from Object.prototype.	 For example, if dest has a custom toString() method,
        // don't overwrite it with the toString() method that source inherited from Object.prototype
        s = source[name];
        if (!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))) {
          dest[name] = s;
        }
      }
      return dest;
    },

    /**
     * @param {*} src
     * @param deep - Whether to perform a deep clone.
     * @returns {*} A clone of the given object.
     */
    clone: function(src, deep) {
      // Adapted from dojo/lang.
      if (!src || !(Types.isObjectLiteral(src) || Types.isArrayLiteral(src)) ||
          Types.isFunction(src)) {
        // null, undefined, any non-object, or function
        return src;	// anything
      }
      if (src.nodeType && 'cloneNode' in src) {
        // DOM Node
        return src.cloneNode(true); // Node
      }
      if (src instanceof Date) {
        // Date
        return new Date(src.getTime());	// Date
      }
      if (src instanceof RegExp) {
        // RegExp
        return new RegExp(src);   // RegExp
      }
      var r, i, l;
      if (Types.isArrayLiteral(src)) {
        // array
        r = [];
        for (i = 0, l = src.length; i < l; ++i) {
          if (i in src) {
            r.push(this.clone(src[i], deep));
          }
        }
        // we don't clone functions for performance reasons
        //		}else if(d.isFunction(src)){
        //			// function
        //			r = function(){ return src.apply(this, arguments); };
      } else {
        // generic objects
        r = src.constructor ? new src.constructor() : {};
      }
      if (deep) {
        for (var key in src) {
          r[key] = this.clone(src[key], deep);
        }
        return r;
      } else {
        return this.mixin(r, src);
      }
    },

    /**
     * @param src
     * @returns {*} A deep clone of the given object.
     */
    cloneDeep: function(src) {
      return this.clone(src, true);
    }

  };
});
